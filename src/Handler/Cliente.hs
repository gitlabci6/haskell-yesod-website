{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}
module Handler.Cliente where

import Import
import Text.Lucius
import Text.Julius
import Yesod.Static
import Settings.StaticFiles

formCliente :: Form Cliente
formCliente = renderDivs $ Cliente 
    <$> areq textField "Nome: "  Nothing 
    <*> areq textField "Cpf:  "  Nothing
    <*> areq intField  "Idade: " Nothing
    
getClienteR :: Handler Html
getClienteR = do
    (widget,_) <- generateFormPost formCliente
    msg <- getMessage
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        addStylesheet (StaticR css_style_css)
        toWidget $(juliusFile "templates/home.julius")
        toWidgetHead $(luciusFile "templates/home.lucius")
        $(whamletFile "templates/cliente.hamlet")

        {- 
        [whamlet|
            $maybe mensa <- msg
                <div>
                    ^{mensa}
            
            <form method=post action=@{ClienteR}>
                ^{widget}
                <input type="submit" value="Cadastrar">
        |]
        -}

postClienteR :: Handler Html
postClienteR = do
    ((result,_),_) <- runFormPost formCliente
    case result of
        FormSuccess cliente -> do
            runDB $ insert cliente
            setMessage [shamlet|
                <div>
                    CLIENTE INSERIDO COM SUCESSO!
            |]
            redirect ClienteR
        _ -> redirect HomeR
    
-- /cliente/perfil/#ClienteId PerfilR GET
-- /clientes ListaCliR GET
-- /cliente/#ClienteId/apagar ApagarCliR POST

-- faz um select * from cliente where id = cid 
-- Se falhar mostra uma pagina de erro.
-- /cliente/perfil/1 => cid = 1
getPerfilR :: ClienteId -> Handler Html
getPerfilR cid = do
    cliente <- runDB $ get404 cid
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        toWidget $(juliusFile "templates/home.julius")
        addStylesheet (StaticR css_style_css)
        $(whamletFile "templates/clienteperfil.hamlet")

getListaCliR :: Handler Html
getListaCliR = do
    -- clientes = [Entity 1 (Cliente "Teste" "..." 45), Entity 2 (Cliente "Teste2" "..." 18), ...]
    clientes <- runDB $ selectList [] [Asc ClienteNome]
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        --datatables
        addStylesheetRemote "https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"
        addScriptRemote "https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"
        addScriptRemote "https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"
        addStylesheetRemote "https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"
        --end datatables
        addStylesheet (StaticR css_style_css)
        toWidget $(juliusFile "templates/clientes.julius")
        toWidgetHead $(luciusFile "templates/home.lucius")
        $(whamletFile "templates/clientes.hamlet")

-- delete from cliente where id = cid
postApagarCliR :: ClienteId -> Handler Html
postApagarCliR cid = do
    runDB $ delete cid
    redirect ListaCliR


