{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}
module Handler.Imovel where

import Import
import Text.Lucius
import Text.Julius
import Yesod.Static
import Settings.StaticFiles

formImovel :: Form Imovel
formImovel = renderDivs $ Imovel 
    <$> areq textField "Dono: "  Nothing 
    <*> areq intField "Área:  "  Nothing
    <*> areq textField  "Endereço: " Nothing
    


getImovelR :: Handler Html
getImovelR = do
    (widget,_) <- generateFormPost formImovel
    msg <- getMessage
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        addStylesheet (StaticR css_style_css)
        toWidget $(juliusFile "templates/home.julius")
        toWidgetHead $(luciusFile "templates/home.lucius")
        $(whamletFile "templates/imovel.hamlet")

        {- 
        [whamlet|
            $maybe mensa <- msg
                <div>
                    ^{mensa}
            
            <form method=post action=@{ImovelR}>
                ^{widget}
                <input type="submit" value="Cadastrar">
        |]
        -}

postImovelR :: Handler Html
postImovelR = do
    ((result,_),_) <- runFormPost formImovel
    case result of
        FormSuccess imovel -> do
            runDB $ insert imovel
            setMessage [shamlet|
                <div>
                    imovel INSERIDO COM SUCESSO!
            |]
            redirect ImovelR
        _ -> redirect HomeR
    

getImoR :: ImovelId -> Handler Html
getImoR cid = do
    imovel <- runDB $ get404 cid
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        toWidget $(juliusFile "templates/home.julius")
        addStylesheet (StaticR css_style_css)
        $(whamletFile "templates/imovelperfil.hamlet")

getListaImoR :: Handler Html
getListaImoR = do
    imoveis <- runDB $ selectList [] [Asc ImovelDono]
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        --datatables
        addStylesheetRemote "https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"
        addScriptRemote "https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"
        addScriptRemote "https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"
        addStylesheetRemote "https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"
        --end datatables
        addStylesheet (StaticR css_style_css)
        toWidget $(juliusFile "templates/imoveis.julius")
        toWidgetHead $(luciusFile "templates/home.lucius")
        $(whamletFile "templates/imoveis.hamlet")

-- delete from imovel where id = cid
postApagarImoR :: ImovelId -> Handler Html
postApagarImoR cid = do
    runDB $ delete cid
    redirect ListaImoR


